# zero-downtime-testing-tool

A testing tool designed to monitor any downtime that occurs during a zero downtime upgrade by continuously performing git operations and sending requests to the readiness?all=1 endpoint.

## How to use

### Configuration file

Create a configuration yaml file inside the `configs` directory. The file should contain a list of rails nodes that requests will be sent to. The list can contain multiple rails nodes or can be for a single load balancer.

Each node listed should contain:

- `name` - this is just used to help identify which machine any given request was sent to and where failures occurred.
- `ip` - can be an ip or url to the rails node to send requests to. Do not include any protocols, ports or slashes.
- `ssh_port` - optional value that needs to be set if git requests shouldn't go to port 22.
- `test_repo` - endpoint of a git repository to clone.

```yaml
rails_nodes:
  -
    name: nw-geo-10k-gitlab-rails-1
    ip: 35.233.90.222
    test_repo: root/test1.git
  -
    name: nw-geo-10k-gitlab-rails-2
    ip: 104.155.99.99
    ssh_port: 1234
    test_repo: root/test2.git
  -
    name: nw-geo-10k-gitlab-rails-3
    ip: 34.78.144.218
    test_repo: root/test3.git
```

### Command line options

| Option                    | Description                                                                            | Default Value |
|---------------------------|----------------------------------------------------------------------------------------|---------------|
| `--environment`           | Name of the config file to load containing GitLab deployment Details                   | n/a           |
| `--log-dir`               | Specify a custom path to a log directory                                               | ./logs        |
| `--git-loop-delay`        | The amount of time (in seconds) to wait between each cycle of git operations           | 10            |
| `--readiness-loop-delay`  | The amount of time (in seconds) to wait between each request to the readiness endpoint | 10            |
| `--insecure-ssh`          | Automatically accept any ECDSA key fingerprints for unauthenticated servers            | n/a           |
| `--output-commands`       | Output the commands that are used for each different node                              | n/a           |
| `--skip-readiness-checks` | Prevent checking of the /readiness?all=1 endpoint                                      | false         |
| `--skip-git-operations`   | Prevent performing git operations on repositories                                      | false         |

> `--insecure-ssh` should be used with caution and only in isolated testing environments. This will use `ssh-keyscan` to accept any new connections without regard for authenticity.

### Running the tests

When running tests the tool is reliant on the presence of a file to determine if the tests should be run or not. The tool will look for
`/tmp/zero-downtime` to exist before running any tests, once the tests are running the tool will not stop until the same file is deleted.
When running an update the time taken can often vary, using the presence of a file allows for the tools execution time to be controlled by a
separate application. You can set the environment variable `GET_TMP_DIR` to change the directory that will be checked for the `zero-downtime` file.

To start the tests and run the tool with all default settings run `./zdt-verifier --environment ./configs/my-environment.yml`.
You can add more command line options to lower the wait time between tests or skip certain tests
`./zdt-verifier --environment ./configs/my-environment.yml --git-loop-delay 3 --skip-readiness-checks`.

### Output

When the tool is finished running it will print a summary of the results to the stdout.
A successful run would look like

```shell
Results:
  Readiness:
    Total number of requests: 135
    Passes: 35
    Failures: 0
  Git Operations:
    Total number of requests: 100
    Passes: 100
    Failures: 0
```

and a run with failures would look similar to

```shell
Results:
  Readiness:
    Total number of requests: 3
    Passes: 0
    Failures:
    Failing Node                                  Error                     Error Level          Currently Updating Node                       Count
    ------------------------------------------------------------------------------------------------------------------------------------------------
    nw-geo-10k-gitlab-rails-1                     502                       error                None                                          1
    nw-geo-10k-gitlab-rails-3                     Connection Timeout        error                None                                          1
    nw-geo-10k-gitlab-rails-2                     Connection Timeout        error                None                                          1
  Git Operations:
    Total number of requests: 15
    Passes: 0
    Failures:
    Failing Node                                  Error                     Error Level          Currently Updating Node                       Count
    ------------------------------------------------------------------------------------------------------------------------------------------------
    nw-geo-10k-gitlab-rails-3                     Clone Failed              error                None                                          5
    nw-geo-10k-gitlab-rails-1                     Clone Failed              error                None                                          5
    nw-geo-10k-gitlab-rails-2                     Clone Failed              error                None                                          5
```

Failures can be analysed more by looking at the relevant logs in the `log-dir` specified. The log is in the format of:

```shell
Start - Loop: <number of loops> - <UTC timestamp> - <Name of the node being update, only set when using with the GitLab Environment Toolkit>
<name specified in config file> - <request that failed / error code> - <Expected/Error, will always be error unless using the GitLab Environment Toolkit>
<stderr from the command run>
```

```shell
Start - Loop: 1 - 2021-04-19 12:57:56 UTC - None
nw-geo-10k-gitlab-rails-3 - Clone Failed - Error
 Cloning into '/tmp/zdt/nw-geo-10k-gitlab-rails-3'...
ssh: connect to host 34.78.144.218 port 22: Operation timed out
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

nw-geo-10k-gitlab-rails-2 - Clone Failed - Error
 Cloning into '/tmp/zdt/nw-geo-10k-gitlab-rails-2'...
ssh: connect to host 104.155.99.99 port 22: Operation timed out
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

nw-geo-10k-gitlab-rails-1 - Clone Failed - Error
 Cloning into '/tmp/zdt/nw-geo-10k-gitlab-rails-1'...
ssh: connect to host 35.233.90.222 port 22: Operation timed out
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

End - 2021-04-19 12:59:21 UTC
```
