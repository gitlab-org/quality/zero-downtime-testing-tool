module GitOperations
  extend self

  require 'open3'
  require_relative 'results'
  require_relative 'test_failure'

  @total_requests = 0
  @passes = 0
  @failures = []

  def total_requests
    @total_requests
  end

  def passes
    @passes
  end

  def failures
    @failures
  end

  def perform_git_operations(log_dir, run_file, tmp_dir, rails_nodes, delay, verbose, url, ssh_port)
    log_file = "#{log_dir}/git.log"
    threads = []
    i = 0

    start_txt = "Start Git Operations"
    puts start_txt if verbose
    File.write(log_file, start_txt)

    filename = "#{SecureRandom.urlsafe_base64}"
    puts filename if verbose
    File.write(log_file, filename)

    while File.exist?(run_file)
      i += 1
      currently_updating_node = File.exist?("#{tmp_dir}/zero-downtime-current") ? File.open("#{tmp_dir}/zero-downtime-current").map(&:chomp)[0] : 'None'

      loop_start_txt = "\nStart - Loop: #{i} - #{Time.now.getutc} - #{currently_updating_node}\n"
      puts loop_start_txt if verbose
      File.write(log_file, loop_start_txt, mode: "a")

      rails_nodes.each do |node|
        threads << Thread.new do
          node['ip'] = url unless url.nil?
          clone_dir = "#{tmp_dir}/zdt/#{node['name']}"
          repo_url = "#{node['ip']}:#{ssh_port}"

          _, clone_err, clone_status = Open3.capture3("git clone ssh://git@#{repo_url}/#{node['test_repo']} #{clone_dir}")
          @passes += 1 if clone_status.success?
          @total_requests += 1
          @failures = Results.parse_git_result(clone_status, log_file, node['name'], 'Clone Success', 'Clone Failed', clone_err, currently_updating_node, @failures, verbose)

          if clone_status.success?
            Open3.capture3("echo '#{i}' >> #{clone_dir}/#{filename}.md")
            Open3.capture3("(cd #{clone_dir} && git add #{filename}.md)")
            Open3.capture3("(cd #{clone_dir} && git commit -m'Add #{i} to #{filename}.md')")

            _, push_err, push_status = Open3.capture3("(cd #{clone_dir} && git push)")
            @passes += 1 if push_status.success?
            @total_requests += 1
            @failures = Results.parse_git_result(push_status, log_file, node['name'], 'Push Success', 'Push Failed', push_err, currently_updating_node, @failures, verbose)
          end

          Open3.capture3("rm -rf #{clone_dir}")
        end
      end

      threads.each(&:join)
      sleep delay
      File.write(log_file, "End - #{Time.now.getutc}\n", mode: "a")
    end
  end
end
