module Results
  extend self

  def parse_rails_result(result_code, result, log_file, node_name, currently_updating_node, passes, failures, verbose)
    if result_code == 200
      passes += 1
      output = "#{node_name} - Success\n"
      puts output if verbose
      File.write(log_file, output, mode: "a")
    else
      allowed_to_fail = node_name == currently_updating_node
      output = "#{node_name} - #{result_code} - #{allowed_to_fail ? 'Expected' : 'Error'}\n #{result}\n"
      puts output if verbose
      File.write(log_file, output, mode: "a")
      failures = parse_result(node_name, currently_updating_node, allowed_to_fail, failures, result_code)
    end

    [passes, failures]
  end

  def parse_git_result(result, log_file, node_name, success_str, error_str, std_error, currently_updating_node, failures, verbose)
    if result.success?
      output = "#{node_name} - #{success_str}\n"
      puts output if verbose
      File.write(log_file, output, mode: "a")
    else
      allowed_to_fail = node_name == currently_updating_node || (currently_updating_node.include? "-gitaly-")
      output = "#{node_name} - #{error_str} - #{allowed_to_fail ? 'Expected' : 'Error'}\n #{std_error}\n"
      puts output if verbose
      File.write(log_file, output, mode: "a")
      failures = parse_result(node_name, currently_updating_node, allowed_to_fail, failures, error_str)
    end

    failures
  end

  def parse_result(node_name, currently_updating_node, allowed_to_fail, failures, result)
    updated = false

    failures.collect do |f|
      next unless f.failing_node == node_name

      if (f.error_level == 'warn' && allowed_to_fail || f.error_level == 'error' && !allowed_to_fail) && f.error == result
        f.count += 1
        updated = true
      end
    end
    failures << TestFailure.new(node_name, result, allowed_to_fail ? 'warn' : 'error', currently_updating_node, 1) unless updated
    failures
  end
end
