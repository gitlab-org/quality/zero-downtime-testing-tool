module Readiness
  extend self

  require 'httparty'
  require_relative 'results'
  require_relative 'test_failure'

  @total_requests = 0
  @passes = 0
  @failures = []

  def total_requests
    @total_requests
  end

  def passes
    @passes
  end

  def failures
    @failures
  end

  def readiness_check_rails_nodes(log_dir, run_file, tmp_dir, rails_nodes, delay, verbose)
    log_file = "#{log_dir}/readiness.log"
    threads = []
    i = 0

    start_txt = "Start Healtcheck"
    puts start_txt if verbose
    File.write(log_file, "Start Healtcheck")

    while File.exist?(run_file)
      i += 1
      currently_updating_node = File.exist?("#{tmp_dir}/zero-downtime-current") ? File.open("#{tmp_dir}/zero-downtime-current").map(&:chomp)[0] : 'None'

      loop_start_txt = "\nStart - Loop: #{i} - #{Time.now.getutc} - #{currently_updating_node}\n"
      puts loop_start_txt if verbose
      File.write(log_file, loop_start_txt, mode: "a")

      rails_nodes.each do |node|
        threads << Thread.new do
          @total_requests += 1
          response = HTTParty.get(URI.parse("http://#{node['ip']}/-/readiness?all=1"))
          @passes, @failures = Results.parse_rails_result(response.code, response, log_file, node['name'], currently_updating_node, @passes, @failures, verbose)
        rescue Errno::ECONNREFUSED
          @passes, @failures = Results.parse_rails_result(503, 'Connection Refused', log_file, node['name'], currently_updating_node, @passes, @failures, verbose)
        rescue Net::OpenTimeout
          @passes, @failures = Results.parse_rails_result(408, 'Connection Timeout', log_file, node['name'], currently_updating_node, @passes, @failures, verbose)
        end
      end

      threads.each(&:join)
      sleep delay
      File.write(log_file, "End - #{Time.now.getutc}\n", mode: "a")
    end
  end
end
