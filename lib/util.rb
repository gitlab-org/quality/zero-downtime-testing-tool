module Util
  extend self

  def register_ssh(rails_nodes)
    rails_nodes.each do |node|
      Open3.capture3("ssh-keyscan #{node['ip']} >> ~/.ssh/known_hosts")
    end

    exit
  end

  def output_commands(rails_nodes)
    rails_nodes.each do |node|
      repo_dir = "#{@zdt_dir}/#{node['name']}"
      repo_url = node['ip'] + (node['ssh_port'].nil? ? '' : ":#{node['ssh_port']}")

      puts "git clone ssh://git@#{repo_url}/#{node['test_repo']} #{repo_dir}"
      puts "curl #{node['ip']}/-/readiness\\?all=1\n"
    end

    exit
  end

  def print_result
    string_format = '%-45s %-25s %-20s %-45s %-5s'
    readiness_report = 0
    git_report = 0

    if Readiness.failures.count.positive?
      readiness_report = "\n          #{format(string_format, 'Failing Node', 'Error', 'Error Level', 'Currently Updating Node', 'Count')}"
      readiness_report += "\n          ------------------------------------------------------------------------------------------------------------------------------------------------"
      Readiness.failures.each do |f|
        readiness_report += "\n          #{format(string_format, f.failing_node, f.error, f.error_level, f.currently_updating_node, f.count)}"
      end
    end

    if GitOperations.failures.count.positive?
      git_report = "\n          #{format(string_format, 'Failing Node', 'Error', 'Error Level', 'Currently Updating Node', 'Count')}"
      git_report += "\n          ------------------------------------------------------------------------------------------------------------------------------------------------"
      GitOperations.failures.each do |f|
        git_report += "\n          #{format(string_format, f.failing_node, f.error, f.error_level, f.currently_updating_node, f.count)}"
      end
    end

    puts %(
      Results:
        Readiness:
          Total number of requests: #{Readiness.total_requests}
          Passes: #{Readiness.passes}
          Failures: #{readiness_report}
        Git Operations:
          Total number of requests: #{GitOperations.total_requests}
          Passes: #{GitOperations.passes}
          Failures: #{git_report}
    )
  end
end
