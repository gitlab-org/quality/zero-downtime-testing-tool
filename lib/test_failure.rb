class TestFailure
  attr_accessor :failing_node, :error, :error_level, :currently_updating_node, :count

  def initialize(*args)
    @failing_node, @error, @error_level, @currently_updating_node, @count = args
  end
end
